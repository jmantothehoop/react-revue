import React from 'react';
import Parser from 'rss-parser';

class News extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      articles: [],
    }
  }


  componentDidMount() {
    const CORS_PROXY = "https://cors-anywhere.herokuapp.com/"
    let parser = new Parser();
    // https://esportsinsider.com/feed/
    parser.parseURL(CORS_PROXY + 'http://esportsobserver.com/feed')
    .then((data) => {
      console.log(data.items);
      this.setState({articles:data.items});
    })
    .catch(console.log)
  }

  render() {
    let articlesToRender = [];

    for(const [index, value] of this.state.articles.slice(3).entries()) {
      articlesToRender.push(
        <div className="rssItem" key={index}>
          <div className="rssImageHeader">
            <img src={value.enclosure.url} alt="imageText" className="rssImage"/>
          </div>
          <div className="rssContent">
            <div className="rssTitle">{value.title}</div>
          </div>
        </div>
      );
    }
    return (
      <div>
        <div className="revueTitle">News</div>
        <div className="rssContainer">
          {articlesToRender}
        </div>
      </div>
    )
  }
}

export default News;