import React from 'react';
import logo from './assets/logo.png';

class Header extends React.Component {
  render() {
    return (
      <div className="logoFlex">
        <img src={logo} alt="Logo" className="logoImage" />
        <div className="revueTitle">Revue Esports</div>
      </div>
    )
  }
}

export default Header;