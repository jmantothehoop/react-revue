import React from 'react';
import ScheduleBlock from './scheduleBlock';
import Header from './header';
import News from './news';

class Revue extends React.Component {
  
  render() {
    const gamesToRender = [];
    const games = ['league-of-legends', 'ow', 'cs-go', 'dota-2'];

    for (const [index, value] of games.entries()) {
      gamesToRender.push(<ScheduleBlock key={index} game={value} />)
    }

    return (
      <div>
        <Header />
        <div className="flexContainer">
          <News />
          {gamesToRender}
        </div>
      </div>
    )
  }
}

export default Revue;