import React from 'react';

class ScheduleBlock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      videoGame: this.props.game,
      currentMatches: [],
      upcomingMatches: []
    }
  }

  componentDidMount() {
    fetch('https://lzhyk2pm41.execute-api.us-east-2.amazonaws.com/default/getGame?gameId=' + this.state.videoGame)
    .then(res => res.json())
    .then((data) => {
      let bodyData = data.body;
      let parsedData = JSON.parse(bodyData);
      let currentMatches = JSON.parse(parsedData.current);
      let upcomingMatches = JSON.parse(parsedData.upcoming);
      this.setState({ 
        currentMatches: currentMatches.Items,
        upcomingMatches: upcomingMatches.Items,
      });
    })
    .catch(console.log)
  }
  
  render() {
    let matchesToRender = [];

    for(const [index, value] of this.state.upcomingMatches.entries()) {
      matchesToRender.push(<div key={index}>{value.begin_at}</div>);
    }

    return(
      <div>
        {matchesToRender}
      </div>
    )
  }

}

export default ScheduleBlock;