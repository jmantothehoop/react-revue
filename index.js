import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import Revue from './revue';


// ========================================

ReactDOM.render(
  <div className="themeContainer">
    <Revue />
  </div>,
  document.getElementById('root')
);
